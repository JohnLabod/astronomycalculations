import math
from scipy import constants as sc

def orbital_period(semimajor_axis, focus_mass, orbit_body_mass):
	# semimajor axis is in kilometers
	# mass is in kilograms
	
	# oribital time in seconds
	orbit_time = 2 * math.pi * math.sqrt((semimajor_axis * 1000) ** 3 / (sc.G * (focus_mass + orbit_body_mass)))
	
	return orbit_time

def orbit_eccentricity(apoapsis, periapsis):
	# apoapsis and periapsis are in kilometers
	return (apoapsis - periapsis) / (apoapsis + periapsis)

def periapsis_distance(semimajor_axis, eccentricity):
	# find closest distance to the focal point on an orbit's transit
	# eccentricity and semimajor_axis are in kilometers
	return semimajor_axis * (1 - eccentricity)

def true_anomaly(eccentricity, semimajor_axis, body_position):
	# semimajor_axis is in kilometers
	# body_position is the position of the orbitting body from reference of the focus (usually a star)
	# body position is an array of two values (x, y) both in kilometers
	
	# get true semi-major axis. distance to the center of the ellipse
	true_semimajor_axis = (semimajor_axis + periapsis_distance(semimajor_axis, eccentricity)) / 2
	# get semi-minor axis
	semiminor_axis = true_semimajor_axis ** 2 * (1 - eccentricity ** 2)
	semiminor_axis = math.sqrt(semiminor_axis)
	
	# get magnitude of the focus to the body position
	focus_to_body_mag = math.sqrt(body_position[0] ** 2 + body_position[1] ** 2)
	
	# get the body position in reference to the center of the ellipse
	center_to_focus = math.sqrt(true_semimajor_axis ** 2 - semiminor_axis ** 2)
	body_position[0] = body_position[0] + center_to_focus
	
	# get magnitude of the ellipse center to the body position
	center_to_body_mag = math.sqrt(body_position[0] ** 2 + body_position[1] ** 2)
	
	
	# find the angle of the body from the center of the ellipse
	angle = math.atan2(body_position[0] * -1, body_position[1]) + math.radians(90)
	if(angle < 0):
		angle = angle + math.radians(360)
	
	# find the angle of the body from the focus point
	anomaly = math.asin(center_to_body_mag / (focus_to_body_mag / math.sin(angle)))
	
	return anomaly
	
	
	
def orbit_distance(semimajor_axis, eccentricity, true_anomaly):
	# finds how far the orbitting body is from its focus point
	# eccentricity and semimajor_axis are in kilometers
	# true_anomaly is the angle of the orbitting body from its periapsis (radians)
	p = semimajor_axis(1 - eccentricity ** 2)
	ta = math.degrees(math.cos(true_anomaly))
	return p / (1 + eccentricity * ta)	

def orbital_angular_velocity(eccentricity, true_anomaly):
	# angle of velocity, relative to the perpendicular to the radial direction
	# true_anomaly is in radians
	angle = e * math.degrees(math.sin(true_anomaly)) / (1 + eccentricity * math.degrees(math.cos(true_anomaly)))
	return math.atan(angle)

def circular_orbit_position(orbit_period, time):
	# only for circular orbits
	# input:
		# orbit_period: how long it takes to orbit the central body (seconds)
		# time: the time since the orbitting body was at position 0 (seconds)
	# output:
		#position: the position of the orbitting body on its circular orbit (radians)
		
	# subtract from time until it fits within orbit_period
	if(time > orbit_period):
		time = time % orbit_period
	
	position = (360 / orbit_period) * time
	
	return math.radians(position)

def orbital_velocity(focus_mass, body_mass, distance, semimajor_axis):
	# input
		# focus_mass - the mass of the focus of the orbit. Like a sun for planets or a planet for satellites (kilograms)
		# body_mass - the orbitting body's mass (kilometers)
		# distance - how far away the body is from the focus (kilometers)
		# semimajor axis is in kilometers
	# output
		# velocity - The velocity of the orbitting body in reference to the focus (km/s)
	standard_gravity = sc.G * (focus_mass + body_mass) / 1000
	velocity = standard_gravity * (2 / distance - 1 / semimajor_axis)
	velocity = math.sqrt(velocity)
	# convert to kilometers per second and return
	return velocity / 1000

# main
true_anomaly(0.2, 12, [0, 2])