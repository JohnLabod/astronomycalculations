def resultant_force(forces):
	# forces must be in a multi-dimenstional array. The first element of the array being
	# the force in newtons and the second element being the angle in radians
	rx = 0
	ry = 0
	for force in forces:
		rx = rx + (force[0] * math.cos(force[1]))
		ry = ry + (force[0] * math.sin(force[1]))
	
	resultant_force = math.sqrt(rx ** 2 + ry ** 2)
	direction = math.atan(ry / rx)
	
	return resultant_force, direction