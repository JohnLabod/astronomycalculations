import math
import numpy

def angle_of_difference(pos_a, pos_b):
	# gets the angle of two vectors. positions are in kilometers
	a_dot = numpy.dot(pos_a[0], pos_a[1])
	b_dot = numpy.dot(pos_b[0], pos_b[1])
	
	a_mag = math.sqrt(pos_a[0]**2 + pos_a[1]**2)
	b_mag = math.sqrt(pos_b[0]**2 + pos_b[1]**2)
	
	return math.acos((pos_a[0] * pos_b[0] + pos_a[1] * pos_b[1]) / (a_mag * b_mag))

def relative_position(focus, object):
	# find the position of the object relative to the focus
	# focus and object are both arrays made of three elements (kilometers)
	coords = []
	for index, val in enumerate(object):
		coords.append(val - focus[index])
	return coords
	
def recrad(rectan):
	# Convert rectangular coordinates to range, right ascension, and declination
	# rectan is an array of three elements (kilometers)
	radius = 0
	lat = 0
	long = 0
	
	# get the sun's ecliptic longitude. The sun's right ascension if the Earth is the observer
	d1 = math.abs(rectan[0])
	d2 = math.abs(rectan[1])
	d1 = math.max(d1,d2)
	d2 = math.abs(rectan[2])
	big = math.max(d1,d2)
	if (big > 0):
		x = rectan[0] / big
		y = rectan[1] / big
		z = rectan[2] / big
		radius = big * math.sqrt(x * x + y * y + z * z)
		lat = atan2(z, math.sqrt(x * x + y * y))
		x = rectan[0]
		y = rectan[1]
		if (x == 0 and y == 0):
			long = 0
		else:
			long = math.atan2(y, x)
	if (long < 0):
		long = long + (math.pi * 2)
	
	return radius, lat, long
	
focus = [6, 2, 0]
object = [4, 4, 0]
print(relative_position(focus, object))