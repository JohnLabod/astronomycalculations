import math
from positions import recrad

def sidereal_time(time, rotation_period, rectan):
	# inputs
		# time - number of seconds since the current frame
		# rotation_period - how much time in takes the planet to fully rotate (seconds)
		# rectan - the 3 element array that describes the location of the rotating body (kilometers)
	day = 0
	if(time > rotation_period):
		day = time % rotation_period
	else:
		day = time
    
	sun_range, sun_ra, sun_dec = recrad(rectan)
	
	# convert to degrees
	sun_lon = math.degrees(sun_ra)
	
	gmst0 = sun_lon + 180
	#Get gmst by converting hours to degrees and adding to gmst at 0h
	gmst = gmst0 + ((day / 60 / 60) * 15)
	
	# change to value under 360 degrees and convert to radians
	return math.radians(gmst % 360)