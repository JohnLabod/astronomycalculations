# AstronomyCalculations

A list of different programs to compute different astronomy calculations

- *orbit.py* Find information about orbit such as eccentricity, orbital period, true anomaly, and velocity
  -  A lot of the formulas were gleaned from [here](http://www.bogan.ca/orbits/kepler/orbteqtn.html)
- *resultant_force.py* Find the resultant force of two or more forces enacted on an object
- *gravity.py* Calculate the amount of force from gravity is being enacted on an object by another object
