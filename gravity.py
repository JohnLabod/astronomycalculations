import math
from scipy import constants as sc

def force_of_gravity(mass_a, mass_b, distance):
	# distance is in kilometers and mass is in kilograms
	
	# calculate force of gravity in newtons
	gravi_force = (mass_a * mass_b) / (distance ** 2)
	gravi_force = sc.G * gravi_force
	
	return gravi_force

def force_to_acc(force, mass):
	acceleration = force / mass
	#acceleration in meter per second squared
	return acceleration

def acc_to_force(mass, acceleration):
	force = mass * acceleration
	#force in Newtons
	return force
